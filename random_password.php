<?php
#PHP生成随机密码的函数
function random_password($len = 7, $mixed_case = false){
        $a = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
        if(!$mixed_case) $a = strtolower($a)
        $out = "";
        for($i = 0; $i < $len; $i++)
                $out .= $a[mt_rand(0, strlen($a))];
        return $out;
}